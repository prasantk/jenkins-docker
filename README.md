# jenkins-docker

# Prepare the volume directory:
- mkdir $HOME/jenkins_home
- chown 1000 $HOME/jenkins_home

# Run the Jenkins container:

 - sudo docker container run --restart always -v $HOME/jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --group-add=$(stat -c %g /var/run/docker.sock) -p 8080:8080 --name jenkins -d prasantk/jenkins:lts