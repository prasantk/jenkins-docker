FROM jenkins/jenkins:lts

RUN /usr/local/bin/install-plugins.sh \
    ansicolor \
    git \
    github \
    workflow-aggregator \
    workflow-multibranch \
    docker-workflow \
    greenballs \
    blueocean \
    htmlpublisher \
    slack \
    mailer \
    ssh-slaves \
    swarm \
    kubernetes-cd \
    email-ext \
    sonar \
    checkstyle

USER root
RUN cd /usr/local/bin && curl -s https://download.docker.com/linux/static/stable/x86_64/docker-18.03.1-ce.tgz | tar xz docker/docker --strip-components=1

# install Maven
# USER root
# RUN apt-get update && apt-get install -y maven
USER jenkins